package za.ac.sun.cs.ingenious.games.ingenious;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

/**
 * BoardInterface
 * 
 * @author steven
 *
 */
public interface IngeniousMinMaxBoardInterface<T extends Action, R> extends GameLogic<IngeniousGameState> {
	
	public boolean full();
	
	public int getHex(Coord position);
	
	
	public int getNumColours();

	public IngeniousAction lastMove();
	

	
	
}