package za.ac.sun.cs.ingenious.core.util.search;

import za.ac.sun.cs.ingenious.core.GameState;
import java.util.List;

public interface TreeNode<S extends GameState, C, P> extends SearchNode<S> {

	// SearchNode interface:
	// TODO: add after completion... And then ask if you should leave or remove
	
	public P getParent();
	
	public void setParent(P parent); //TODO: check to see if function needed? Should parents be able to change? (e.g. in other search algorithms?)
	
	public List<C> getChildren();
	
	public void setChildren(List<C> children); //TODO: check if function needed? Should children list be able to change to different list?
	
	public void addChild(C child);
	
	public void addChildren(List<C> newChildren);
	
}

// TODO: comments!
