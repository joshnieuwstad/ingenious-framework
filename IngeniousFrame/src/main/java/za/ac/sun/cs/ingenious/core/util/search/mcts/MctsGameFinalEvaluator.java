package za.ac.sun.cs.ingenious.core.util.search.mcts;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Used for returning the score for each player once a simulation has ended. 
 * Every domain that wants to use a MCTS search in the framework should have a 
 * class implementing this.
 * 
 * @author Karen Laubscher
 * 
 * @param <S> The type of GameState that this evaluator can evaluate.
 */
public interface MctsGameFinalEvaluator<S extends GameState> extends GameFinalEvaluator<S> {

    // GameFinalEvaluator interface:
	// public double[] getScore(S forState);
	
	/**
	 * This method provides the win/loss/draw scores and values will be used as 
	 * the results of the MCTS simulations. The value of the MCTS node depend on 
	 * the sum of all simulation results
	 *
	 * It is advised to return 1 for a win, -1 for a loss and 0 for a draw.
	 * Otherwise normalised results are also a good choice.
	 * 
	 * Note that the behaviour of this method is not defined if the provided 
	 * state is not terminal.
	 *
	 * @param forState	The terminal state for which the score is required.
	 * @return double[]	An array where a[i] is the player i's score in the provided terminal state.
	 *
	 */
	public double[] getMctsScore(S forState);
	
	/**
	 * This method returns the value of a "win".
	 *
	 * @return double	The value representing a win.
	 */
	public double getWinValue();
	
	/**
	 * This method returns the value of a "loss"
	 *
	 * @return double	The value representing a loss.
	 */
	public double getLossValue();
	
}
