/**
 * package za.ac.sun.cs.ingenious.games.cardGames.core.actions provide the most typical actions you need for card games.
 * 
 * Please note: PlayCardAction and DrawCardAction are very similar. But for a game logic it might come in very handy to distinguish between the two move types. 
 * 
 * The HiddenDrawCardAction is designed to let players know that another player had to draw a card but without letting them know which card.
 * 
 * @author Joshua Wiebe
 * 
 */

package za.ac.sun.cs.ingenious.games.cardGames.core.actions;