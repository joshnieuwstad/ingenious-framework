package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MctsCMCNodeExtensionParallel<S extends GameState> implements MctsNodeExtensionParallelInterface {

    // lock for ID
    private final ReadWriteLock lockID = new ReentrantReadWriteLock();
    private final Lock readLockID = lockID.readLock();

    // node composition for which the wrapper is applied
    MctsCMCNodeExtensionComposition contextualNodeExtensionBasic;

    /**
     * Constructor for this parallel wrapper for the CMC node extension
     */
    public MctsCMCNodeExtensionParallel() {
        contextualNodeExtensionBasic = new MctsCMCNodeExtensionComposition();
    }


    /** These methods are used for the Rave implementation, the interface requires them to be present **/
    public void setUp(MctsNodeComposition node) {}
    public <S extends GameState> void addChild(MctsNodeTreeParallel<S> child){}
    public <S extends GameState> void addChildren(List<MctsNodeTreeParallel<S>> newChildren){}

    /**Prints to log the win to visit rate of all children as well as the value attributed to that move according
     * to the relevant implementation (This value would be used during selection to choose an action) */
    public <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void logPossibleMoves(N child) {
        // empty since values not relevant to print
    }

    /**
     * @return the id of the player to play at the node for which this extension object relates
     */
    public String getID() {
        readLockID.lock();
        try {
            return contextualNodeExtensionBasic.getID();
        } finally {
            readLockID.unlock();
        }
    }

}
