package za.ac.sun.cs.ingenious.search.mcts;

import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsNode;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public interface MctsTreeParallelNode<S extends GameState, C, P> extends MctsNode<S, C, P> {
	
	double getVirtualLoss();
	
	void applyVirtualLoss(double lossValue);
	
	void restoreVirtualLoss(double lossValue);
	
	void incVisitVirtualLoss(double lossValue);
	
	void addValueVirtualLoss(double lossValue, double add);
	
	void readLock();
	
	void readUnlock();
	
	void writeLock();
	
	void writeUnlock();

	int getDepth();

	void logPossibleMoves();
		
}

// TODO: comments!
