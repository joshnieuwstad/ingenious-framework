Some example scripts for testing / using the commandline interface provided
by the Ingenious framework.

Make sure you run: gradle jarFat
in the root of the repository before using these scripts.

Order of running scripts:
1. start_server.sh <port number>
3. connect_clients.sh <number of games to run> <enhancement for player 1> <enhancement for player 2> <port number> \<number of threads for both players to play on>

The configuration for the game is given in the 
BMDefault.json
